
public class Rectangle {
	int sideA;
	int sideB;
	
	public Rectangle(int sideA,int sideB){
		this.sideA = sideA;
		this.sideB = sideB;
	}
	
	public int area(){
		return sideA * sideB;
	}
	
	public int perimeter(){
		return 2 * (sideA + sideB);
	}
}
