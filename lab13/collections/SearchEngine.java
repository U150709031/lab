package collections;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SearchEngine {

	public Map<String, Set<String>> index = new HashMap<>();
	
	public void index(String url) throws Exception{
		
		Document doc = Jsoup.connect(url).get();
		String content = doc.text();
		Pattern p = Pattern.compile("\\b\\w+\\b");
		Matcher m = p.matcher(content);
		
		while(m.find()) {
			String word = m.group();
			//System.out.println(word);
			
			Set<String> links = index.get(word);
			if(links == null) {
				links = new HashSet<String>();
				links.add(url);
				index.put(word,links);
				
			}else {
				links.add(url);
			}
		}
	}

    public Set<String> search(String input) {
        return index.get(input);
    }
}