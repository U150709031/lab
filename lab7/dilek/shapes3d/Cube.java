package dilek.shapes3d;

import dilek.shapes.Square;

public class Cube extends Square {

	public Cube(int side) {
		super(side);

		System.out.println("Cube is being created");
	}

	public int area() {
		return 6 * (side * side);
	}
	
	public int volume() {
		return side * side * side;
	}
	
	@Override
	public String toString() {
		return "side = " + side + " " + "area = " + area() + " " + "volume = " + volume();
	}
}
