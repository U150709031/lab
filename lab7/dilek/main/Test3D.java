package dilek.main;

import dilek.shapes3d.*;

public interface Test3D {
	
	public static void main(String[] args) {
		Cylinder cylinder = new Cylinder(6, 10);
		System.out.println(cylinder.toString());
		
		Cube cube = new Cube(5);
		System.out.println(cube.toString());
	}
		

}